#!/usr/bin/php
<?php
require_once 'Ramsey/Uuid/autoload.php';
$uuid = Ramsey\Uuid\Uuid::uuid4()->toString();
$valid = Ramsey\Uuid\Uuid::isValid($uuid);
exit($valid ? 0 : 1);
